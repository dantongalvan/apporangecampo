<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
        <title>Herramienta Linea Directa</title>		
        <link rel="shortcut icon" href="./img/favicon.ico" type="image/x-icon">
        <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.css" />
               
        
        <script type='text/javascript' src='js/jquery.js'></script>
        <script type='text/javascript' src='js/jquery-filestyle.js'></script>

        <link rel="stylesheet" type="text/css" href="css/reset.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery-filestyle.css" />
        
        <!-- libreria jquery para los tabs (Pestañas) -->
        <link rel="stylesheet" href="js/jquery-ui-themes-1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="css/estilos.css" />
        <script src="js/jquery-1.12.4.js"></script>
        <script src="js/jquery-ui.js"></script>
        <script src="js/bootstrap.min.js"></script> 
        <script type="text/javascript">
            
        </script>

    </head> 
    
<body>
   
        <div>
            <div id="TituloCabecera">
                <h1>Orange Campo</h1>
            </div>
        <div id="Formulario" >

            <button type="button" class="btn btn-info btn-circle btn-xl"><i class="glyphicon glyphicon-plus"></i>AÑADIR</button>
            <button type="button" class="btn btn-danger btn-circle btn-xl"><i class="glyphicon glyphicon-remove"></i>ELIMINAR</button>
            <button type="button" class="btn btn-warning btn-circle btn-xl"><i class="glyphicon glyphicon-pencil"></i>MODIFICAR</button>
        </div>
        </div>

<div class="container">

    <div class="container-fluid" style="margin-top: 30px">

        <div class="panelLeft col-md-9">
            <div id="myTabHolder" class="span10 offset1 tabbable tabs-left">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#Tiendas" data-toggle="tab">
                            <span class="account-type">Tiendas</span><br/>
                        </a>
                    </li>
                    <li class="">
                        <a href="#TiendasCEM" data-toggle="tab">
                            <span class="account-type">Tiendas CEM</span><br/>

                        </a>
                    </li>
                    <li class="">
                        <a href="#Oleadas" data-toggle="tab">
                            <span class="account-type">Oleadas</span><br/>
                        </a>
                    </li>
                    <li class="">
                        <a href="#Proveedores" data-toggle="tab">
                            <span class="account-type">Proveedores</span><br/>
                        </a>
                    </li>
                    <li class="">
                        <a href="#Auditores" data-toggle="tab">
                            <span class="account-type">Auditores</span><br/>
                        </a>
                    </li>
                    <li class="">
                        <a href="#Usuarios" data-toggle="tab">
                            <span class="account-type">Usuarios</span><br/>
                        </a>
                    </li>
                    <li class="">
                        <a href="#RLime" data-toggle="tab">
                            <span class="account-type">Tabla Lime</span><br/>
                        </a>
                    </li>
                    <li class="">
                        <a href="#RSie" data-toggle="tab">
                            <span class="account-type">Tabla SIE</span><br/>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="Tiendas">
                        <div class="row col-md-6 col-md-offset-2 custyle">
                            <table class="table table-striped custab">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Parent ID</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                                    <tr>
                                        <td>1</td>
                                        <td>News</td>
                                        <td>News Cate</td>
                                        <td class="text-center"><a class='btn btn-info btn-xs' href="#"><span class="glyphicon glyphicon-edit"></span> Edit</a> <a href="#" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Del</a></td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Products</td>
                                        <td>Main Products</td>
                                        <td class="text-center"><a class='btn btn-info btn-xs' href="#"><span class="glyphicon glyphicon-edit"></span> Edit</a> <a href="#" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Del</a></td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Blogs</td>
                                        <td>Parent Blogs</td>
                                        <td class="text-center"><a class='btn btn-info btn-xs' href="#"><span class="glyphicon glyphicon-edit"></span> Edit</a> <a href="#" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Del</a></td>
                                    </tr>
                            </table>
                            </div>

                    </div>
                    <div class="tab-pane" id="TiendasCEM">
                        <div>
                            <p>TiendasCEM selected.</p>
                        </div>
                    </div>
                    <div class="tab-pane" id="Oleadas">
                        <div>
                            <p>Oleadas selected.</p>
                        </div>
                    </div>
                    <div class="tab-pane" id="Proveedores">
                        <div>
                            <p>Proveedores selected.</p>
                        </div>
                    </div>
                    <div class="tab-pane" id="Auditores">
                        <div>
                            <p>Auditores selected.</p>
                        </div>
                    </div>
                    <div class="tab-pane" id="Usuarios">
                        <div>
                            <p>Usuarios selected.</p>
                        </div>
                    </div>
                    <div class="tab-pane" id="RLime">
                        <div>
                            <p>RLime selected.</p>
                        </div>
                    </div>
                    <div class="tab-pane" id="RSie">
                        <div>
                            <p>RSie selected.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="panelRight col-md-3">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Tareas Pendientes</h3>
                </div>
                <ul class="list-group">
                    <a href="#" class="list-group-item">Tarea 1</a>
                    <a href="#" class="list-group-item">Tarea 2</a>
                    <a href="#" class="list-group-item">Tarea 3</a>
                    <a href="#" class="list-group-item">Tarea 4</a>
                </ul>

                    <button type="button" class="btn btn-info btn-circle btn-xl"><i class="glyphicon glyphicon-time"> </i>NUEVA</button> 
                    <button type="button" class="btn btn-danger btn-circle btn-xl"><i class="glyphicon glyphicon-remove"> </i>BORRAR</button>

                <div class="proveedores">
                    <div class="radio radio-primary">
                        <input type="radio" name="radio1" id="radio1" value="option1">
                        <label for="radio1">
                            Proveedor 1
                        </label>
                    </div>
                    <div class="radio radio-primary">
                        <input type="radio" name="radio1" id="radio2" value="option2">
                        <label for="radio2">
                            Proveedor 2
                        </label>
                    </div>
                    <div class="radio radio-primary">
                        <input type="radio" name="radio1" id="radio3" value="option3">
                        <label for="radio3">
                            Proveedor 3
                        </label>
                    </div>
                    <div class="radio radio-primary">
                        <input type="radio" name="radio1" id="radio4" value="option4">
                        <label for="radio4">
                            Proveedor 4
                        </label>
                    </div>
                    <button type="button" class="btn btn-success btn-circle btn-xl"><i class="glyphicon glyphicon-envelope"> </i>ENVIAR</button>   
                </div>
        </div>
    </div>
</div>


</body>
</html>

 



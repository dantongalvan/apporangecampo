<?php

/**
 * Clase para el manejo del Log
 *
 * @author diego.castello
 */
class Log {

    static $LOG_DO_TABLA= "LineaDirecta.log";


    private $ficheroHandler;

    function __construct($ficheroLog){
        $rutaFicheroLog = "log/".$ficheroLog;
        $this->ficheroHandler = fopen($rutaFicheroLog, "a");
        fwrite($this->ficheroHandler, PHP_EOL);
    }
    function guardarLog($linea){
        //echo "Linea Log: ".$linea."<br>"; // para mostrar log en el navegador

        fwrite($this->ficheroHandler, $linea.PHP_EOL);
    }

    function __destruct(){
        fclose($this->ficheroHandler);
    }
    
}

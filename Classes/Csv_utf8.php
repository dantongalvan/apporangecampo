<?php

class Csv {
	
	function __construct(){
			
	}
	
	/*
	* Función que genera un archivo CSV separado por | y sin comillas para los textos con espacios
	*/
	function generarCsv($datos,$nombreFichero){
		$arrayFormateado = array();
		$buscar=array(chr(13).chr(10), "\r\n", "\n", "\r",";");
		$reemplazar=array(" - ", " - ", " - ", " - ",".");
		


		
		foreach($datos as $key1 => $encuesta) {
			foreach($encuesta as $key2 => $valor) {	
				$arrayFormateado[$key1][$key2] =str_ireplace($buscar,$reemplazar,$valor);			
			}
		}		
		
		// comentar foreach o la siguiente línea
		// $arrayFormateado = $datos;

		//$rutaFichero = "ficheros/csv/".$nombreFichero; 	
		$ficheroCSV = fopen($nombreFichero, "w");
		if ($ficheroCSV == false) {
			return false;
		} else {
			//volcamos el contenido del array en formato csv
			foreach($arrayFormateado as $encuesta) {

				$linea = array ();
				foreach ($encuesta as $clave3 => $dato) {
					$linea[] = $dato;
				}
				$strigLinea =  (implode(';', $linea));
				fwrite($ficheroCSV, $strigLinea);
				fwrite($ficheroCSV,"\r\n"); //salto linea
				//fputcsv($ficheroCSV,$encuesta,"|"); //no funciona como se requiere, añade comillas a los textos que tienen espacios

			}
			fflush($ficheroCSV);
			fclose($ficheroCSV);
			return true;
		}
		
		
	}
}
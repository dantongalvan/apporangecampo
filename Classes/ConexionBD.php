<?php

/**
 * Clase para el manejo de las conexiones con BD
 */
    
class ConexionBD {
    
    // Produccion
    // estudiosCuotasDb
    const HOST_ESTUDIOS = 'mysqllimesurvey01.madisonmk.local';
    const USER_ESTUDIOS = 'app.cuotas';
    const PASS_ESTUDIOS = 'kowa0EuJDpCR26GpKO3X';
    const BD_ESTUDIOS = 'estudiosCuotasDb';


	public $conexion;

	
    private $GESTOR_LOG;

    public function __construct() {
       $this->conexion = mysqli_connect(self::HOST_ESTUDIOS, self::USER_ESTUDIOS, self::PASS_ESTUDIOS, self::BD_ESTUDIOS);
    }

	private function desconectarEstudiosCuotasBD(){
        if($this->conexion){
            mysqli_close($this->conexion);
        }
    }
   
    public function __destruct() {        
		$this->desconectarEstudiosCuotasBD();
    }

}


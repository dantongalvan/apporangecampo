<?php

// Incluyo la libreria SFTP de phpseclib
    set_include_path(get_include_path() . PATH_SEPARATOR . 'libs/phpseclib'); 
    require_once 'Net/SFTP.php';

/**
 * Clase para el manejo de las conexiones FTP
 */
class ConexionSFTP {
    
    // Produccion
        const HOST_PRO = 'sftp.madisonmk.com';
        const USER_PRO = 'sftp.orange.cem';
        const PASS_PRO = 'Gis91TV5';
        const DIRECTORIO_FICHERO_CEM_PRO = '/sftp.orange.cem/';
        //const DIRECTORIO_FICHERO_SIE_PRO = '/ficheros/csv/';
    
    // Definitivo
        private $HOST;
        private $USER;
        private $PASS;
        private $DIRECTORIO_FICHERO_CEM;
        //private $DIRECTORIO_FICHERO_SIE;
    
        
    private $conSFTP;
    
    public function __construct() {
		$this->HOST = self::HOST_PRO;
		$this->USER = self::USER_PRO;
		$this->PASS = self::PASS_PRO;
		$this->DIRECTORIO_FICHERO_CEM = self::DIRECTORIO_FICHERO_CEM_PRO;
		//$this->DIRECTORIO_FICHERO_SIE = self::DIRECTORIO_FICHERO_SIE_PRO;           
        $this->conectarSFTP();
    }

    private function conectarSFTP() {
        $this->conSFTP = new Net_SFTP($this->HOST);
    }
    
    public function loginSFTP() {
        if ($this->conSFTP->login($this->USER,$this->PASS)) {
            return true;
        }else{
            return false;
        }
    }
	
	public function entregarFichero($rutaFicheroLocal, $nombreFichero) {
        if($this->conSFTP->put($this->DIRECTORIO_FICHERO_CEM.$nombreFichero, $rutaFicheroLocal.$nombreFichero, NET_SFTP_LOCAL_FILE)){	
            return true;
        }
        return false;
    }
    
    
    public function __destruct() {
        $this->conSFTP->disconnect();
    }

}

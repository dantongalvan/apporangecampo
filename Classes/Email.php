<?php

// Incluyo libreria PHPMailer para el envio de email
require("PHPMailer/PHPMailerAutoload.php");
require_once("Net/IMAP.php");

/**
 * Clase que gestiona el envio de email
 *  
 */
class Email {

    const HOST = 'correo.madisonmk.com';
    const USER = 'app.limesurvey250';
    const PASS = 'jy7W3tEvYp';
    const PUERTO = 25;
	const FROM = 'orangemystery@madisonmk.com';

    
    private $destinosNotificacion;
    private $destinosNotificacionOcultos;
    private $destinosError;
    private $errorRecepcion;
    private $mensajeErrorRecepcion;

    function __construct() {
        // Destinatarios 
        $this->destinosNotificacion = array(
            // Madison //            
            //"alejandro.peinador@madisonmk.com",
			//"carlos.zamorano@madisonmk.com",
			"victor.cardillo@madisonmk.com", 
			"patricia.romo@madisonmk.com", 
			"ruben.asenjo@madisonmk.com", 
            "diego.anton@madisonmk.com",
            //"victor.nino@madisonmk.com",
            //"cesarmiguel.perez@madisonmk.com"

        );
		// Destinatarios 
        $this->destinosNotificacionSinEquipoCampo = array(
            // Madison //
            
            /*"alejandro.peinador@madisonmk.com",
            "rosa.vigo@madisonmk.com",
            "laura.pereza@madisonmk.com",*/
            /*"patricia.romo@madisonmk.com",*/
        );

        // Destinatarios copia oculta
        $this->destinosNotificacionOcultos = array(
            //"alejandro.peinador@madisonmk.com",
        );

        // Destinatarios ERROR
        $this->destinosError = array(
			//"alejandro.peinador@madisonmk.com", //...
			//"carlos.zamorano@madisonmk.com",
            "diego.anton@madisonmk.com",
            //"victor.nino@madisonmk.com",
            //"cesarmiguel.perez@madisonmk.com"
        );

    }
	

/**
    * Envia email con la confirmación de la generación del archivo CSV 
    */

    public function enviarEmailCsvCem($archivo ) {

        $asunto = "[MADISON - ORANGE] Notificación archivo Enviado a CEM ";
        
        $cuerpoEmail  = "Hola,<br><br>";
        $cuerpoEmail .= "El archivo <code>".$archivo."</code>, con las alegaciones, se ha enviado correctamente al SFTP de CEM<br><br>";
        $cuerpoEmail .= "Un saludo del equipo del proyecto AlegacionesOrange<br> ";
        
        $resultado = $this->enviarEmail($asunto, $cuerpoEmail, $this->destinosNotificacion, $this->destinosNotificacionOcultos);

        return $resultado;
    }



	/**
	* Envia email con ERROR genérico
	*/
	public function enviarEmailErrorGenérico($fecha ) {

        $asunto = "[MADISON - ORANGE] ERROR en Generar Alegaciones Orange ";
		
		$cuerpoEmail  = "Hola,<br><br>";
        $cuerpoEmail .= "Se ha producido algún tipo de error al ejecutar el proceso AlegacionesOrange en ";
        $cuerpoEmail .= " la fecha ".$fecha."'.<br><br>";
		$cuerpoEmail .= "Un saludo";
		
        $resultado = $this->enviarEmail($asunto, $cuerpoEmail, $this->destinosError, $this->destinosNotificacionOcultos);

        return $resultado;
    }

    private function enviarEmail($asunto, $cuerpoEmail, $destinosVisibles, $destinosOcultos = array()) {
        $mail = new PHPMailer();

        // Iniciar la validación por SMTP:
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->Host = self::HOST;
        $mail->Username = self::USER;
        $mail->Password = self::PASS;
        $mail->Port = self::PUERTO;

        // Iniciamos una conexión con el SMTP.
        $mail->From = self::FROM;   // Desde donde enviamos (Para mostrar)
        $mail->FromName = self::FROM;

        // Incluyo las direcciones de envio visibles
        foreach ($destinosVisibles as $destino) {
            $mail->AddAddress($destino);
        }

        // Incluyo las direcciones de envio en copia oculta
        foreach ($destinosOcultos as $destino) {
            $mail->AddBCC($destino); // Copia oculta
            //$mail->AddBCC($destino);	// Copia visible
        }

        $mail->IsHTML(true); // El correo se envía como HTML
        $mail->Subject = $asunto; // Asunto del email.			
        $mail->Body = $cuerpoEmail; // Mensaje a enviar
        $mail->CharSet = 'UTF-8';

        // Envio el correo
        if ($mail->Send()) {
            return true;
        }
        return false;
    }

 
 
}
